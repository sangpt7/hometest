<?php

namespace Controllers;

class Controller
{
    protected function success($data)
    {
        response(200, $data);
    }

    protected function error($data, $statusCode = 500)
    {
        response($statusCode, $data);
    }
}

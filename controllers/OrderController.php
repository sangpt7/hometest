<?php

namespace Controllers;

use App\Interfaces\IOrder;
use App\Interfaces\IOrderItem;

class OrderController extends Controller
{
    /** @var IOrder */
    public $order;

    public function __construct(IOrder $order)
    {
        $this->order = $order;
    }

    public function calculateGrossPriceAction()
    {
        try {
            $input = json_decode(file_get_contents('php://input'), true);
            $input = $this->validateCalculateGrossPriceFromClientInput($input);
            $orderItemInputList = $input['order_items'];
            foreach ($orderItemInputList as $orderItemInput) {
                $orderItem = resolve(IOrderItem::class, [
                    'amzPrice' => $orderItemInput['amz_price'] ?? 0,
                    'weight' => $orderItemInput['weight'] ?? 0,
                    'height' => $orderItemInput['height'] ?? 0,
                    'width' => $orderItemInput['width'] ?? 0,
                    'depth' => $orderItemInput['depth'] ?? 0,
                ]);
                $this->order->addOrderItem($orderItem);
            }

            $this->success($this->order->calculateGrossPrice());
        } catch (\Exception $e) {
            $this->error($e->getMessage(), 400);
        }
    }

    private function validateCalculateGrossPriceFromClientInput($input)
    {
        if (!isset($input['order_items']) || !is_array($input['order_items'])) {
            throw new \Exception('Array of order item is not set');
        }

        return $input;
    }
}


<?php

require('bootstrap.php');

$requestString = trim($_SERVER['REQUEST_URI'], " \t\n\r\0\x0B/");
if (!$requestString) {
    echo "Welcome to home test";
    die;
}

$urlParams = explode('/', array_shift(explode('?', $requestString)));
$controllerName = 'Controllers\\' . ucfirst(array_shift($urlParams)) . 'Controller';
$actionName = lcfirst(array_shift($urlParams)) . 'Action';
$argumentsRequest = $urlParams;

if (!class_exists($controllerName)) {
    redirectNotFound();
}

$controller = $container->make($controllerName);
if (!method_exists($controller, $actionName)) {
    redirectNotFound();
}

$classMethod = new ReflectionMethod($controllerName,$actionName);
if (count($argumentsRequest) < $classMethod->getNumberOfRequiredParameters()
    || count($argumentsRequest) > $classMethod->getNumberOfParameters()) {
    redirectNotFound();
}

return $argumentsRequest ? $classMethod->invokeArgs($controller, $argumentsRequest) : $classMethod->invoke($controller);


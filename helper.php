<?php

if (!function_exists('config')) {
    function config($key)
    {
        global $config;
        return $config[$key] ?? null;
    }
}

if (!function_exists('redirectNotFound')) {
    function redirectNotFound()
    {
        $root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
        header("Location: {$root}");
        die;
    }
}

if (!function_exists('resolve')) {
    function resolve($className, $arguments = [])
    {
        global $container;
        return $container->make($className, $arguments);
    }
}

if (!function_exists('response')) {
    function response($statusCode, $data = NULL)
    {
        $status = array(
            200 => 'OK',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error'
        );

        header("HTTP/1.1 " . $statusCode . " " . $status[$statusCode]);
        header("Content-Type: application/json");
        echo json_encode($data);
        die;
    }
}


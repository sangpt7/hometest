<?php


namespace App\Models;


use App\Interfaces\IFee;
use App\Interfaces\IShippingFee;

class ShippingFee implements IShippingFee
{
    /** @var IFee[] */
    private $listFee;

    public function addFees(IFee ...$fees)
    {
        foreach ($fees as $fee) {
            $this->listFee[] = $fee;
        }
        $this->sortFee();
    }

    public function getFee():? IFee
    {
        if (empty($this->listFee)) {
            return null;
        }

        return ($this->listFee[0]);
    }

    private function sortFee()
    {
        $reducedListFee = array_map(function (IFee $v) {
            return $v->getValue();
        }, $this->listFee);

        arsort($reducedListFee);

        $tempListFee = [];
        foreach (array_keys($reducedListFee) as $key) {
            $tempListFee[] = $this->listFee[$key];
        }

        $this->listFee = $tempListFee;
    }
}

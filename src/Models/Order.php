<?php


namespace App\Models;


use App\Interfaces\IOrder;
use App\Interfaces\IOrderItem;

class Order extends Model implements IOrder
{
    private $listOrderItem = [];

    public function addOrderItem(IOrderItem $orderItem)
    {
        $this->listOrderItem[] = $orderItem;
    }

    public function calculateGrossPrice()
    {
        return array_sum(array_map(function (IOrderItem $orderItem) {
            return $orderItem->getPrice();
        }, $this->listOrderItem));
    }
}

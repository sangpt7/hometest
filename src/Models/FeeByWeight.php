<?php


namespace App\Models;


use App\Interfaces\IFee;
use App\Interfaces\IFeeByWeight;

class FeeByWeight implements IFee, IFeeByWeight
{
    private $weight;

    public function __construct($weight = 0)
    {
        $this->setWeight($weight);
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    public function getValue()
    {
        return $this->weight * (config('weight_coefficient') ?? 0);
    }
}

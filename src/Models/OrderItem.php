<?php


namespace App\Models;


use App\Interfaces\IFeeByDimension;
use App\Interfaces\IFeeByWeight;
use App\Interfaces\IItem;
use App\Interfaces\IOrderItem;
use App\Interfaces\IShippingFee;

class OrderItem extends Model implements IOrderItem
{
    /** @var IItem */
    public $item;

    /** @var IShippingFee */
    public $shippingFee;

    private $amzPrice;

    public function __construct(IItem $item, IShippingFee $shippingFee, $amzPrice = null, $weight = null, $height = null, $width = null, $depth = null)
    {
        $this->item = $item;
        $this->shippingFee = $shippingFee;
        $this->amzPrice = $amzPrice;

        $this->item->setWeight($weight);
        $this->item->setHeight($height);
        $this->item->setWidth($width);
        $this->item->setDepth($depth);

        $this->addFee();
    }

    private function addFee()
    {
        $this->shippingFee->addFees(
            resolve(IFeeByWeight::class, [
                'weight' => $this->item->getWeight()
            ]),
            resolve(IFeeByDimension::class, [
                'height' => $this->item->getHeight(),
                'width' => $this->item->getWidth(),
                'depth' => $this->item->getDepth(),
            ])
        );
    }

    private function getFee()
    {
        return $this->shippingFee->getFee()->getValue();
    }

    public function getPrice()
    {
        return $this->amzPrice + $this->getFee();
    }
}

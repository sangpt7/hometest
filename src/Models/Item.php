<?php


namespace App\Models;


use App\Interfaces\IItem;

class Item extends Model implements IItem
{
    private $weight;
    private $width;
    private $height;
    private $depth;

    public function __construct($weight = null, $width = null, $height = null, $depth = null)
    {
        $this->setWeight($weight);
        $this->setWidth($width);
        $this->setHeight($height);
        $this->setDepth($depth);
    }

    public function getDepth()
    {
        return $this->depth;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function setDepth($depth)
    {
        $this->depth = $depth;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    public function setWidth($width)
    {
        $this->width = $width;
    }
}

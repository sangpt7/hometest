<?php


namespace App\Models;


use App\Interfaces\IFee;
use App\Interfaces\IFeeByDimension;

class FeeByDimension implements IFee, IFeeByDimension
{
    private $width;
    private $height;
    private $depth;

    public function __construct($width = null, $height = null, $depth = null)
    {
        $this->setWidth($width);
        $this->setHeight($height);
        $this->setDepth($depth);
    }

    public function setWidth($width)
    {
        $this->width = $width;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function setDepth($depth)
    {
        $this->depth = $depth;
    }

    public function getValue()
    {
        return $this->width * $this->height * $this->depth * (config('dimension_coefficient') ?? 0);
    }
}

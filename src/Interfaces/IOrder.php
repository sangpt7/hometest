<?php


namespace App\Interfaces;


interface IOrder
{
    public function addOrderItem(IOrderItem $orderItem);
    public function calculateGrossPrice();
}

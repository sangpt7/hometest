<?php


namespace App\Interfaces;


interface IShippingFee
{
    public function addFees(IFee ...$fees);

    public function getFee():? IFee;
}

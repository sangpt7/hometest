<?php


namespace App\Interfaces;


interface IItem
{
    public function getDepth();

    public function getHeight();

    public function getWeight();

    public function getWidth();

    public function setDepth($depth);

    public function setHeight($height);

    public function setWeight($weight);

    public function setWidth($width);
}

<?php


namespace App\Interfaces;


interface IOrderItem
{
    public function getPrice();
}

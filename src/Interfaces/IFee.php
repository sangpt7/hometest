<?php


namespace App\Interfaces;


interface IFee
{
    public function getValue();
}

<?php

return [
    \App\Interfaces\IOrder::class => Di\create(\App\Models\Order::class),
    \App\Interfaces\IOrderItem::class => Di\create(\App\Models\OrderItem::class)->constructor(
        Di\get(\App\Interfaces\IItem::class),
        Di\get(\App\Interfaces\IShippingFee::class)
    ),
    \App\Interfaces\IItem::class => Di\create(\App\Models\Item::class),
    \App\Interfaces\IShippingFee::class => Di\create(\App\Models\ShippingFee::class),
    \App\Interfaces\IFeeByWeight::class => Di\create(\App\Models\FeeByWeight::class),
    \App\Interfaces\IFeeByDimension::class => Di\create(\App\Models\FeeByDimension::class),
];

<?php

namespace Tests\Controllers;

use App\Interfaces\IOrder;
use App\Interfaces\IOrderItem;
use PHPUnit\Framework\TestCase;

class OrderControllerTest extends TestCase
{
    /**
     * @param $input
     * @param $expect
     *
     * @dataProvider calculateGrossPriceActionFullInputSet
     * @test
     */
    public function calculateGrossPriceActionFullInputTest($input, $expect)
    {
        /** @var IOrder $order */
        $order = resolve(IOrder::class);
        foreach ($input as $orderItemInput) {
            $orderItem = resolve(IOrderItem::class, [
                'amzPrice' => $orderItemInput['amz_price'] ?? 0,
                'weight' => $orderItemInput['weight'] ?? 0,
                'height' => $orderItemInput['height'] ?? 0,
                'width' => $orderItemInput['width'] ?? 0,
                'depth' => $orderItemInput['depth'] ?? 0,
            ]);
            $order->addOrderItem($orderItem);
        }

        $result = $order->calculateGrossPrice();

        $this->assertEquals($expect, $result);
    }

    public static function calculateGrossPriceActionFullInputSet()
    {
        return [
            // fully, fee by weight is bigger
            [
                [
                    [
                        'amz_price' => 10,
                        'weight' => 10,
                        'width' => 1,
                        'height' => 1,
                        'depth' => 1
                    ]
                ],
                120
            ],
            // fully, fee by dimension is bigger
            [
                [
                    [
                        'amz_price' => 10,
                        'weight' => 10,
                        'width' => 1,
                        'height' => 1,
                        'depth' => 20
                    ]
                ],
                230
            ]
        ];
    }
}
